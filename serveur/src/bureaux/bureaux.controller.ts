import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  NotFoundException, UseGuards, UseInterceptors, ClassSerializerInterceptor
} from '@nestjs/common';
import { BureauxService } from './bureaux.service';
import { CreateBureauDto } from './dto/create-bureau.dto';
import { UpdateBureauDto } from './dto/update-bureau.dto';
import {
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { Bureau } from './entities/bureau.entity';
import Role from '../utilisateurs/role.enum';
import { RoleGuard } from '../auth/guards/role.guard';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { FiltrerBureauxDto } from './dto/filtrer-bureaux-dto';

@ApiTags('bureaux')
@Controller('bureaux')
export class BureauxController {
  constructor(private readonly bureauxService: BureauxService) {}

  @ApiCreatedResponse({
    type: Bureau,
  })
  @Post()
  @UseInterceptors(ClassSerializerInterceptor)
  async create(@Body() createBureauDto: CreateBureauDto) {
    return this.bureauxService.create(createBureauDto);
  }

  @ApiOkResponse({
    type: [Bureau],
  })
  @Get()
  @UseInterceptors(ClassSerializerInterceptor)
  async findAll() {
    return this.bureauxService.findAll();
  }

  @ApiOkResponse({
    type: Bureau,
  })
  @ApiNotFoundResponse()
  @Get(':id')
  @UseInterceptors(ClassSerializerInterceptor)
  async findOne(@Param('id') id: string) {
    return this.bureauxService.findOne(+id).catch((erreur) => {
      throw new NotFoundException();
    });
  }

  @ApiOkResponse({
    type: Bureau,
  })
  @ApiNotFoundResponse()
  @Patch(':id')
  @UseGuards(JwtAuthGuard, RoleGuard(Role.Admin))
  @UseInterceptors(ClassSerializerInterceptor)
  async update(@Param('id') id: string, @Body() updateBureauDto: UpdateBureauDto) {
    return this.bureauxService.update(+id, updateBureauDto).catch((erreur) => {
      throw new NotFoundException();
    });
  }

  @ApiOkResponse()
  @ApiNotFoundResponse()
  @Delete(':id')
  @UseGuards(JwtAuthGuard, RoleGuard(Role.Admin))
  async remove(@Param('id') id: string) {
    return this.bureauxService.remove(+id).catch((erreur) => {
      throw new NotFoundException();
    });
  }

  @ApiOkResponse({
    type: [Bureau],
  })
  @Post('filtrer')
  async filtrer(@Body() filtrerBureauxDto: FiltrerBureauxDto) {
    return this.bureauxService.filtrer(filtrerBureauxDto);
  }
}
