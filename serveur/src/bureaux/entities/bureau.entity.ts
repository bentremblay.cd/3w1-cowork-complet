import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { BaseEntity, Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Reservation } from '../../reservations/entities/reservation.entity';
import { Etage } from '../../etages/entities/etage.entity';

@Entity()
export class Bureau extends BaseEntity {
  @ApiModelProperty({
    example: 1,
    description: "L'identifiant du bureau",
  })
  @PrimaryGeneratedColumn()
  id: number;

  @ApiModelProperty({
    example: '01-100',
    description:
      "Le code du bureau, au format 00-000. Les deux premiers chifres correspondent à l'étage, alors que les trois derniers au numéro de bureau/pièce",
  })
  @Column({ type: 'text', unique: true })
  code: string;

  @ApiModelProperty({
    required: false,
    example: 'Magnifique bureau partagé avec des inconnus.',
    description: 'La description du bureau',
  })
  @Column({ type: 'text', nullable: true })
  description?: string;

  @ApiModelProperty({
    example: true,
    description: 'Si le bureau se trouve dans un espace partagé ou privé.',
  })
  @Column({ type: 'boolean', default: false, width: 1 })
  estPrive: boolean;

  @ApiModelProperty({
    example: 2,
    description:
      'Si le bureau se trouve dans un ilot de plusieurs bureaux ou une pièce, précise la capacité.',
  })
  @Column({ type: 'int', default: 1 })
  capacite: number;

  @Column({
    type: 'longtext',
    nullable: true,
  })
  image: string;

  @OneToMany(() => Reservation, (reservation) => reservation.bureau)
  reservations: Reservation[];

  @ManyToOne(() => Etage, (etage) => etage.bureaux, {
    eager: true,
  })
  etage: Etage;
}
