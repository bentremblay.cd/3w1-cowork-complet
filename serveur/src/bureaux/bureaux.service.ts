import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateBureauDto } from './dto/create-bureau.dto';
import { UpdateBureauDto } from './dto/update-bureau.dto';
import { Bureau } from './entities/bureau.entity';
import { DeepPartial } from 'typeorm';
import { Buffer } from 'buffer';
import { Etage } from '../etages/entities/etage.entity';
import { FiltrerBureauxDto } from './dto/filtrer-bureaux-dto';

@Injectable()
export class BureauxService {
  async create(createBureauDto: CreateBureauDto) {
    //return Bureau.save(createBureauDto as DeepPartial<Bureau>);

    const { code, description, capacite, estPrive, etageId, image } = createBureauDto;
    const bureau = Bureau.create({
      code,
      description,
      capacite,
      estPrive,
      image,
    });

    const etage = await Etage.findOneByOrFail({ id: etageId });
    bureau.etage = etage;

    return bureau.save();
  }

  async findAll() {
    return Bureau.find();
  }

  async findOne(id: number) {
    return Bureau.findOneOrFail({ where: { id: id } });
  }

  async update(id: number, updateBureauDto: UpdateBureauDto) {
    const bureau = await Bureau.findOneOrFail({ where: { id: id } });
    Object.assign(bureau, updateBureauDto);

    if (updateBureauDto.etageId) {
      const etage = await Etage.findOneByOrFail({
        id: updateBureauDto.etageId,
      });
      bureau.etage = etage;
    }

    return bureau.save();
  }

  async remove(id: number) {
    const bureau = await Bureau.findOneOrFail({ where: { id: id } });
    return bureau.remove();
  }

  async filtrer(filtrerBureauxDto: FiltrerBureauxDto) {
    const { etage } = filtrerBureauxDto;

    if(!etage) return Bureau.find();

    return Bureau.findBy({ etage: { id: etage } });
  }
}
