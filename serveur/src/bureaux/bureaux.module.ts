import { Module } from '@nestjs/common';
import { BureauxService } from './bureaux.service';
import { BureauxController } from './bureaux.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Bureau } from './entities/bureau.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Bureau])],
  controllers: [BureauxController],
  providers: [BureauxService],
})
export class BureauxModule {}
