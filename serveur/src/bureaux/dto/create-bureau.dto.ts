import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsNumber, IsOptional, IsString, Matches } from 'class-validator';

export class CreateBureauDto {
  @ApiProperty({
    example: '03-400',
    description: 'Le code du bureau, sous le format 00-000',
  })
  @IsString()
  @Matches('^[0-9]{2}-[0-9]{3}$')
  code: string;

  @ApiProperty({
    example: 1,
    description: "Id de l'étage du bureau",
  })
  etageId: number;

  @ApiProperty({
    example: 'Bureau plutôt moche',
    description: 'La description du bureau',
    required: false,
  })
  @IsString()
  @IsOptional()
  description?: string;

  @ApiProperty({
    example: true,
    description: 'Si le bureau est dans un espace privé',
  })
  @IsBoolean()
  estPrive: boolean;

  @ApiProperty({
    example: 4,
    description: "La capacité de l'espace ou de l'ilot où se trouve le bureau.",
  })
  @IsNumber()
  capacite: number;

  @ApiProperty({
    description: 'Image du bureau',
    required: false,
  })
  @IsString()
  image?: string;
}
