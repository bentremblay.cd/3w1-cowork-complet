import { ApiProperty } from '@nestjs/swagger';

export class FiltrerBureauxDto {
  @ApiProperty({
    example: '1',
    description: "Le ID de l'étage à filtrer",
  })
  etage: number;
}
