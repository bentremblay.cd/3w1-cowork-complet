import { PartialType } from '@nestjs/swagger';
import { CreateBureauDto } from './create-bureau.dto';

export class UpdateBureauDto extends PartialType(CreateBureauDto) {}
