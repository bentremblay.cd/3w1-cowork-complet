import * as bcrypt from 'bcrypt';
import { ConflictException, Injectable, InternalServerErrorException } from '@nestjs/common';
import { CreateUtilisateurDto } from './dto/create-utilisateur.dto';
import { DeepPartial } from 'typeorm';
import { Utilisateur } from './entities/utilisateur.entity';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class UtilisateursService {
  constructor(private jwtService: JwtService) {}

  async create(createUtilisateurDto: CreateUtilisateurDto) {
    const utilisateur = Utilisateur.create(
      createUtilisateurDto as DeepPartial<Utilisateur>,
    );

    utilisateur.motDePasse = bcrypt.hashSync(utilisateur.motDePasse, 10);

    await utilisateur.save().catch((erreur) => {
      if (erreur.code === 'ER_DUP_ENTRY')
        throw new ConflictException('Ce courriel existe déjà');

      throw new InternalServerErrorException();
    });

    const access_token = this.jwtService.sign({
      sub: utilisateur.id,
      courriel: utilisateur.courriel,
      role: utilisateur.role,
    });
    return { access_token };
  }
}
