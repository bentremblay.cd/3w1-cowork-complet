import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsString, MaxLength, MinLength } from 'class-validator';

export class CreateUtilisateurDto {
  @ApiProperty({
    example: 'Benoit',
    description: "Prénom de l'utilisateur",
  })
  @IsString()
  prenom: string;

  @ApiProperty({
    example: 'Tremblay',
    description: "Nom de l'utilisateur",
  })
  @IsString()
  nom: string;

  @ApiProperty({
    example: 'benoit.tremblay@cegepdrummond.ca',
    description: "Courriel de l'utilisateur",
  })
  @IsString()
  @IsEmail()
  courriel: string;

  @ApiProperty({
    example: '12345678',
    description: "Mot de passe de l'utilisateur",
  })
  @IsString()
  @MinLength(8)
  @MaxLength(20)
  motDePasse: string;
}
