import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne, OneToMany
} from 'typeorm';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { Exclude } from 'class-transformer';
import Role from '../role.enum';
import { Reservation } from '../../reservations/entities/reservation.entity';

@Entity()
export class Utilisateur extends BaseEntity {
  @ApiModelProperty({
    example: '1',
    description: "L'identifiant de l'utilisateur",
  })
  @PrimaryGeneratedColumn()
  id: number;

  @ApiModelProperty({
    example: 'Benoit',
    description: "Le prénom de l'utilisateur",
  })
  @Column()
  prenom: string;

  @ApiModelProperty({
    example: 'Tremblay',
    description: "Le nom de l'utilisateur",
  })
  @Column()
  nom: string;

  @ApiModelProperty({
    example: 'benoit.tremblay@cegepdrummond.ca',
    description: "Le courriel de l'utilisateur",
  })
  @Column({ unique: true })
  courriel: string;

  @Column()
  @Exclude()
  motDePasse?: string;

  @Column({
    type: 'enum',
    enum: Role,
    default: Role.Utilisateur,
  })
  role: Role;

  @OneToMany(() => Reservation, (reservation) => reservation.utilisateur)
  reservations: Reservation[];

  @CreateDateColumn()
  date_creation?: Date;

  @UpdateDateColumn()
  date_modification?: Date;
}
