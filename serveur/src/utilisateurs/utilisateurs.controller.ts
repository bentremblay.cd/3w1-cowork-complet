import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  Post,
  Req,
  UseGuards,
  UseInterceptors
} from '@nestjs/common';
import { UtilisateursService } from './utilisateurs.service';
import { ApiCreatedResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { ReponseConnexionDto } from '../auth/dto/reponse-connexion.dto';
import { CreateUtilisateurDto } from './dto/create-utilisateur.dto';
import { Utilisateur } from './entities/utilisateur.entity';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { Request } from 'express';

@ApiTags('utilisateurs')
@Controller('utilisateurs')
export class UtilisateursController {
  constructor(private utilisateursService: UtilisateursService) {}

  @ApiCreatedResponse({
    type: ReponseConnexionDto,
  })
  @Post()
  @UseInterceptors(ClassSerializerInterceptor)
  async create(@Body() createUtilisateurDto: CreateUtilisateurDto) {
    return this.utilisateursService.create(createUtilisateurDto);
  }

  @ApiOkResponse({
    type: Utilisateur,
  })
  @Get('profil')
  @UseGuards(JwtAuthGuard)
  async profil(@Req() req: Request) {
    return req.user as Utilisateur;
  }
}
