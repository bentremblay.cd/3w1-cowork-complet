import { Module } from '@nestjs/common';
import { UtilisateursController } from './utilisateurs.controller';
import { UtilisateursService } from './utilisateurs.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Utilisateur } from './entities/utilisateur.entity';
import { JwtModule } from '@nestjs/jwt';
import { JwtConstantes } from '../auth/jwt-constantes';

@Module({
  imports: [
    TypeOrmModule.forFeature([Utilisateur]),
    JwtModule.register({
      secret: JwtConstantes.secret,
      signOptions: { expiresIn: '24h' },
    }),
  ],
  controllers: [UtilisateursController],
  providers: [UtilisateursService],
})
export class UtilisateursModule {}
