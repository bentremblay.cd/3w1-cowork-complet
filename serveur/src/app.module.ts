import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BureauxModule } from './bureaux/bureaux.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';
import { UtilisateursModule } from './utilisateurs/utilisateurs.module';
import { ReservationsModule } from './reservations/reservations.module';
import { EtagesModule } from './etages/etages.module';

@Module({
  imports: [
    BureauxModule,
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'gestion_bd',
      password: 'bd3N3_1234!',
      database: 'cowork_complet',
      autoLoadEntities: true,
      synchronize: true,
      logging: true,
    }),
    AuthModule,
    UtilisateursModule,
    ReservationsModule,
    EtagesModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
