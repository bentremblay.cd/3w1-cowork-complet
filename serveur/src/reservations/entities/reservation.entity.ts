import {
  BaseEntity,
  Column,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Bureau } from '../../bureaux/entities/bureau.entity';
import { IsDate, IsEmail, IsOptional, IsString } from 'class-validator';
import { Exclude, Transform, Type } from 'class-transformer';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { Utilisateur } from '../../utilisateurs/entities/utilisateur.entity';
import * as Util from 'util';

@Entity()
export class Reservation extends BaseEntity {
  @ApiModelProperty({
    example: 1,
    description: "L'identifiant de la réservation",
  })
  @PrimaryGeneratedColumn()
  id: number;

  @ApiModelProperty({
    example: '2022-10-23',
    description: 'La date de la réservation',
  })
  @Column()
  @IsDate()
  @Type(() => Date)
  date: Date;

  @ApiModelProperty({
    example: 'Benoit',
    description: 'Prénom du client pour la réservation',
  })
  @Column()
  @IsString()
  prenom: string;

  @ApiModelProperty({
    example: 'Tremblay',
    description: 'Nom du client pour la réservation',
  })
  @Column()
  @IsString()
  nom: string;

  @ApiModelProperty({
    example: 'benoit.tremblay@cegepdrummond.ca',
    description: 'Courriel du client pour la réservation',
  })
  @Column()
  @IsEmail()
  courriel: string;

  @ApiModelProperty({
    example: '123 De la longue rue',
    description: 'Adresse du client pour la réservation',
  })
  @Column()
  @IsString()
  adresse: string;

  @ApiModelProperty({
    example: 'Drummondville',
    description: 'Ville du client pour la réservation',
  })
  @Column()
  @IsString()
  ville: string;

  @ApiModelProperty({
    example: 'J0C 1K0',
    description: 'Code postal du client pour la réservation',
  })
  @Column()
  @IsString()
  codePostal: string;

  @ApiModelProperty({
    example: "J'aurais une précision à apporter...",
    description: 'Commentaire optionnel du client',
  })
  @Column({ type: 'text', nullable: true })
  @IsString()
  @IsOptional()
  commentaire?: string;

  @ManyToOne(() => Bureau, (bureau) => bureau.reservations, {
    eager: true,
  })
  bureau: Bureau;

  @ManyToOne(() => Utilisateur, (utilisateur) => utilisateur.reservations, {
    nullable: true,
    eager: true,
  })
  utilisateur?: Utilisateur;
}
