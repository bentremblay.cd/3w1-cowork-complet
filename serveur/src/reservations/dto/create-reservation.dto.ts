import { Column, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { IsEmail, IsString } from 'class-validator';
import { Bureau } from '../../bureaux/entities/bureau.entity';
import { ApiProperty } from '@nestjs/swagger';

export class CreateReservationDto {
  @ApiProperty({
    example: '2022-10-32',
    description: 'Date de la réservation'
  })
  date: Date;

  @ApiProperty({
    example: 'Benoit',
    description: 'Prénom du client pour la réservation'
  })
  @ApiProperty()
  prenom: string;

  @ApiProperty({
    example: 'Tremblay',
    description: 'Nom du client pour la réservation',
  })
  nom: string;

  @ApiProperty({
    example: 'benoit.tremblay@cegepdrummond.ca',
    description: 'Courriel du client pour la réservation',
  })
  courriel: string;

  @ApiProperty({
    example: '123 De la longue rue',
    description: 'Adresse du client pour la réservation',
  })
  adresse: string;

  @ApiProperty({
    example: 'Drummondville',
    description: 'Adresse du client pour la réservation',
  })
  ville: string;

  @ApiProperty({
    example: 'J0C 1K0',
    description: 'Code postal du client pour la réservation'
  })
  codePostal: string;

  @ApiProperty({
    example: 'Ceci est un commentaire!',
    description: 'Commentaire optionnel pour la réservation'
  })
  commentaire?: string;
}
