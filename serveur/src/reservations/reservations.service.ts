import { ConflictException, ForbiddenException, Injectable, Res } from '@nestjs/common';
import { Reservation } from './entities/reservation.entity';
import * as Util from 'util';
import { Utilisateur } from '../utilisateurs/entities/utilisateur.entity';
import Role from '../utilisateurs/role.enum';

@Injectable()
export class ReservationsService {
  async findAll() {
    return Reservation.find({ order: { date: 'DESC' } });
  }

  async findOne(id: number) {
    return Reservation.findOneOrFail({ where: { id: id } });
  }

  async remove(id: number, utilisateur: Utilisateur) {
    const reservation = await Reservation.findOneOrFail({ where: { id: id } });

    if (
      utilisateur.role != Role.Admin &&
      reservation.utilisateur?.id != utilisateur.id
    )
      throw new ForbiddenException();

    return reservation.remove();
  }

  async mesReservations(utilisateur: Utilisateur) {
    return Reservation.find({
      where: {
        utilisateur: {
          id: utilisateur.id,
        },
      },
      order: {
        date: 'DESC',
      },
    });
  }
}
