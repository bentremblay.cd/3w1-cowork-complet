import { Module } from '@nestjs/common';
import { ReservationsService } from './reservations.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Reservation } from './entities/reservation.entity';
import { ReservationsBureauxController } from './reservations-bureaux/reservations-bureaux.controller';
import { ReservationsController } from './reservations.controller';
import { ReservationsBureauxService } from './reservations-bureaux/reservations-bureaux.service';

@Module({
  imports: [TypeOrmModule.forFeature([Reservation])],
  controllers: [ReservationsBureauxController, ReservationsController],
  providers: [ReservationsService, ReservationsBureauxService],
})
export class ReservationsModule {}
