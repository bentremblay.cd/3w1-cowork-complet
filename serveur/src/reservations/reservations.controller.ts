import { Controller, Delete, Get, Param, Req, UseGuards } from '@nestjs/common';
import { ReservationsService } from './reservations.service';
import { RoleGuard } from '../auth/guards/role.guard';
import Role from '../utilisateurs/role.enum';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { ApiNotFoundResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { Reservation } from './entities/reservation.entity';
import { Request } from 'express';
import { Utilisateur } from '../utilisateurs/entities/utilisateur.entity';

@ApiTags('reservations')
@Controller('reservations')
export class ReservationsController {
  constructor(private reservationsService: ReservationsService) {}

  @ApiOkResponse({
    type: [Reservation],
  })
  @Get()
  @UseGuards(JwtAuthGuard, RoleGuard(Role.Admin))
  async findAll() {
    return this.reservationsService.findAll();
  }

  /**
   * L'ordre d'apparition de cette fonction est importante.
   * Elle est déclarée avant la route dynamique :id qui la suit.
   * L'inverse ferait en sorte que pour /reservations/mes-reservations,
   * la portion 'mes-reservations' serait perçu comme un id.
   * Toujours déclarer les routes statiques avant.
   * @param req
   */
  @ApiOkResponse()
  @Get('mes-reservations')
  @UseGuards(JwtAuthGuard)
  async mesReservations(@Req() req: Request) {
    return this.reservationsService.mesReservations(req.user as Utilisateur);
  }

  @ApiOkResponse({
    type: Reservation,
  })
  @ApiNotFoundResponse()
  @Get(':id')
  async findOne(@Param('id') id: string) {
    return this.reservationsService.findOne(+id);
  }

  @ApiOkResponse()
  @ApiNotFoundResponse()
  @Delete(':id')
  @UseGuards(JwtAuthGuard)
  async remove(@Param('id') id: string, @Req() req: Request) {
    return this.reservationsService.remove(+id, req.user as Utilisateur);
  }
}
