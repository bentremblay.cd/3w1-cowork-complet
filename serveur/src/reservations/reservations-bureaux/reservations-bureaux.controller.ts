import { Body, Controller, Get, Param, Post, Req, UseGuards } from '@nestjs/common';
import { CreateReservationDto } from '../dto/create-reservation.dto';
import { ReservationsBureauxService } from './reservations-bureaux.service';
import { ApiCreatedResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { Etage } from '../../etages/entities/etage.entity';
import { Reservation } from '../entities/reservation.entity';
import { Request } from 'express';
import { Utilisateur } from '../../utilisateurs/entities/utilisateur.entity';
import { JwtAuthGuard } from '../../auth/guards/jwt-auth.guard';
import { JwtAuthGuardOptionnel } from '../../auth/guards/jwt-auth-guard-optionnel.guard';

@ApiTags('reservations-bureaux')
@Controller('bureaux/:bureauId/reservations')
export class ReservationsBureauxController {
  constructor(private reservationsBureauxService: ReservationsBureauxService) {}

  @ApiCreatedResponse({
    type: Reservation,
  })
  @Post()
  @UseGuards(JwtAuthGuardOptionnel)
  async create(
    @Param('bureauId') bureauId: string,
    @Body() createReservationDto: CreateReservationDto,
    @Req() req: Request,
  ) {
    return this.reservationsBureauxService.create(
      +bureauId,
      createReservationDto,
      req.user as Utilisateur,
    );
  }

  @ApiOkResponse({
    type: Reservation,
  })
  @Get()
  async findAll(@Param('bureauId') bureauId: string) {
    return this.reservationsBureauxService.findAll(+bureauId, 7);
  }
}
