import { Test, TestingModule } from '@nestjs/testing';
import { ReservationsBureauxController } from './reservations-bureaux.controller';

describe('ReservationsBureauxController', () => {
  let controller: ReservationsBureauxController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ReservationsBureauxController],
    }).compile();

    controller = module.get<ReservationsBureauxController>(ReservationsBureauxController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
