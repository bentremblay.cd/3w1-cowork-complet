import { Test, TestingModule } from '@nestjs/testing';
import { ReservationsBureauxService } from './reservations-bureaux.service';

describe('ReservationsBureauxService', () => {
  let service: ReservationsBureauxService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ReservationsBureauxService],
    }).compile();

    service = module.get<ReservationsBureauxService>(ReservationsBureauxService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
