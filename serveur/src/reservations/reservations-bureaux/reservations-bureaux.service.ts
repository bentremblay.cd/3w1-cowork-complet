import { ConflictException, Injectable } from '@nestjs/common';
import { CreateReservationDto } from '../dto/create-reservation.dto';
import { Bureau } from '../../bureaux/entities/bureau.entity';
import { Reservation } from '../entities/reservation.entity';
import { DeepPartial } from 'typeorm';
import { Utilisateur } from '../../utilisateurs/entities/utilisateur.entity';

@Injectable()
export class ReservationsBureauxService {
  async create(
    bureauId: number,
    createReservationDto: CreateReservationDto,
    utilisateur: Utilisateur,
  ) {
    const bureau = await Bureau.findOneOrFail({ where: { id: bureauId } });

    /**
     * Si une réservation existe déjà pour la date et le bureau,
     * on ne crée pas de nouvelle réservation (ConflictException).
     */
    const reservationExistante = await Reservation.findOneBy({
      date: createReservationDto.date,
      bureau: { id: bureau.id },
    });

    if (reservationExistante) throw new ConflictException();

    const reservation = createReservationDto as DeepPartial<Reservation>;
    reservation.bureau = bureau;

    if (utilisateur) reservation.utilisateur = utilisateur;

    return Reservation.save(createReservationDto as DeepPartial<Reservation>);
  }

  async findAll(bureauId: number, limit?: number) {
    const bureau = await Bureau.findOneOrFail({ where: { id: bureauId } });
    return Reservation.find({
      where: {
        bureau: {
          id: bureau.id,
        },
      },
      order: {
        date: 'DESC',
      },
      take: limit,
    });
  }

  findOne(id: number) {
    return Reservation.findOneOrFail({ where: { id: id } });
  }
}
