import { AuthGuard } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';

@Injectable()
export class JwtAuthGuardOptionnel extends AuthGuard('jwt') {
  handleRequest(err, user, info, context) {
    return user;
  }
}
