import { Body, Controller, Post } from '@nestjs/common';
import { InfosConnexionDto } from './dto/infos-connexion.dto';
import { AuthService } from './auth.service';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { ReponseConnexionDto } from './dto/reponse-connexion.dto';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @ApiOkResponse({
    type: ReponseConnexionDto,
  })
  @Post()
  connexion(@Body() infosConnexionDto: InfosConnexionDto) {
    return this.authService.connexion(infosConnexionDto);
  }
}
