import * as bcrypt from 'bcrypt';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InfosConnexionDto } from './dto/infos-connexion.dto';
import { Utilisateur } from '../utilisateurs/entities/utilisateur.entity';
import { ReponseConnexionDto } from './dto/reponse-connexion.dto';

@Injectable()
export class AuthService {
  constructor(private jwtService: JwtService) {}

  async connexion(
    infosConnexionDto: InfosConnexionDto,
  ): Promise<ReponseConnexionDto> {
    const utilisateur = await Utilisateur.findOneOrFail({
      where: {
        courriel: infosConnexionDto.courriel,
      },
    });

    if (
      !utilisateur ||
      !bcrypt.compareSync(infosConnexionDto.motDePasse, utilisateur.motDePasse)
    ) {
      throw new UnauthorizedException(
        "Nom d'utilisateur ou mot de passe incorrect",
      );
    }

    const access_token = this.jwtService.sign({
      sub: utilisateur.id,
      courriel: utilisateur.courriel,
      role: utilisateur.role,
    });
    return { access_token };
  }
}
