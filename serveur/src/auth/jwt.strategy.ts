import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { JwtPayloadDto } from './dto/jwt-payload.dto';
import { Utilisateur } from '../utilisateurs/entities/utilisateur.entity';
import { UnauthorizedException } from '@nestjs/common';
import { JwtConstantes } from './jwt-constantes';

export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: JwtConstantes.secret,
    });
  }

  async validate(payload: JwtPayloadDto): Promise<Utilisateur> {
    const { courriel } = payload;
    const utilisateur = await Utilisateur.findOne({
      where: {
        courriel,
      },
    });

    if (!utilisateur) {
      throw new UnauthorizedException();
    }
    return utilisateur;
  }
}
