import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsString, MaxLength, MinLength } from 'class-validator';

export class InfosConnexionDto {
  @ApiProperty({
    example: 'benoit.tremblay@cegepdrummond.ca',
    description: "Courriel utilisé pour l'authentification",
  })
  @IsEmail()
  courriel: string;

  @ApiProperty({
    example: '12345678',
    description: "Mot de passe de l'utilisateur",
  })
  @IsString()
  @MinLength(8)
  @MaxLength(20)
  motDePasse: string;
}
