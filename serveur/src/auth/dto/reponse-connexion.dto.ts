import { ApiProperty } from '@nestjs/swagger';

export class ReponseConnexionDto {
  @ApiProperty({
    example: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IlJvZ2VyIFdpbGNvIiwiaWF0IjoxNTE2MjM5MDIyfQ.2fA2T6_92g3xomqL0IjrW_6XMBafS9uU1LRGRaAUmKE',
    description: "Le jeton JWT associé à l'utilisateur",
  })
  access_token: string;
}
