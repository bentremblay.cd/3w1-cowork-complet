import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards } from '@nestjs/common';
import { EtagesService } from './etages.service';
import { CreateEtageDto } from './dto/create-etage.dto';
import { UpdateEtageDto } from './dto/update-etage.dto';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { RoleGuard } from '../auth/guards/role.guard';
import Role from '../utilisateurs/role.enum';
import { ApiCreatedResponse, ApiNotFoundResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { Etage } from './entities/etage.entity';

@ApiTags('etages')
@Controller('etages')
export class EtagesController {
  constructor(private readonly etagesService: EtagesService) {}

  @ApiCreatedResponse({
    type: Etage,
  })
  @Post()
  @UseGuards(JwtAuthGuard, RoleGuard(Role.Admin))
  create(@Body() createEtageDto: CreateEtageDto) {
    return this.etagesService.create(createEtageDto);
  }

  @ApiOkResponse({
    type: [Etage],
  })
  @Get()
  findAll() {
    return this.etagesService.findAll();
  }

  @ApiOkResponse({
    type: Etage,
  })
  @ApiNotFoundResponse()
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.etagesService.findOne(+id);
  }

  @ApiOkResponse({
    type: Etage,
  })
  @ApiNotFoundResponse()
  @Patch(':id')
  @UseGuards(JwtAuthGuard, RoleGuard(Role.Admin))
  update(@Param('id') id: string, @Body() updateEtageDto: UpdateEtageDto) {
    return this.etagesService.update(+id, updateEtageDto);
  }

  @ApiOkResponse()
  @ApiNotFoundResponse()
  @Delete(':id')
  @UseGuards(JwtAuthGuard, RoleGuard(Role.Admin))
  remove(@Param('id') id: string) {
    return this.etagesService.remove(+id);
  }
}
