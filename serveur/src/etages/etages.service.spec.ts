import { Test, TestingModule } from '@nestjs/testing';
import { EtagesService } from './etages.service';

describe('EtagesService', () => {
  let service: EtagesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EtagesService],
    }).compile();

    service = module.get<EtagesService>(EtagesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
