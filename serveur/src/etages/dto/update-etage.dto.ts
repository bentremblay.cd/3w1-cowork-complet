import { PartialType } from '@nestjs/swagger';
import { CreateEtageDto } from './create-etage.dto';

export class UpdateEtageDto extends PartialType(CreateEtageDto) {}
