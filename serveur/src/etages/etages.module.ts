import { Module } from '@nestjs/common';
import { EtagesService } from './etages.service';
import { EtagesController } from './etages.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Etage } from './entities/etage.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Etage])],
  controllers: [EtagesController],
  providers: [EtagesService],
})
export class EtagesModule {}
