import { Test, TestingModule } from '@nestjs/testing';
import { EtagesController } from './etages.controller';
import { EtagesService } from './etages.service';

describe('EtagesController', () => {
  let controller: EtagesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [EtagesController],
      providers: [EtagesService],
    }).compile();

    controller = module.get<EtagesController>(EtagesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
