import { Injectable } from '@nestjs/common';
import { CreateEtageDto } from './dto/create-etage.dto';
import { UpdateEtageDto } from './dto/update-etage.dto';
import { Etage } from './entities/etage.entity';
import { DeepPartial } from 'typeorm';

@Injectable()
export class EtagesService {
  async create(createEtageDto: CreateEtageDto) {
    return Etage.save(createEtageDto as DeepPartial<Etage>);
  }

  async findAll() {
    return Etage.find();
  }

  async findOne(id: number) {
    return Etage.findOneByOrFail({ id: id });
  }

  async update(id: number, updateEtageDto: UpdateEtageDto) {
    const etage = await Etage.findOneByOrFail({ id: id });
    Object.assign(etage, updateEtageDto);

    return etage.save();
  }

  async remove(id: number) {
    const etage = await Etage.findOneByOrFail({ id: id });

    return etage.remove();
  }
}
