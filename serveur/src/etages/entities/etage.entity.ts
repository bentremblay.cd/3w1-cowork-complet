import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Bureau } from '../../bureaux/entities/bureau.entity';

@Entity()
export class Etage extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nom: string;

  @OneToMany(() => Bureau, (bureau) => bureau.etage)
  bureaux: Bureau[];
}
