# Cowork serveur

L'application serveur (API) pour Cowork.

## Base de données

### Export

Un export de la base de données est fourni avec quelques données de test. Vous pouvez l'importer via phpmyadmin (Import dans le menu principal). Vous n'avez pas nécessairement à l'utiliser, mais cela vous évite de créer des données par vous-même.

### Infos de connexion

Remplacez au besoin la base de données et les infos de connexion à la BD si ces dernières sont différentes de votre côté:

```ts
TypeOrmModule.forRoot({
  type: 'mysql',
  host: 'localhost',
  port: 3306,
  username: 'gestion_bd',
  password: 'bd3N3_1234!',
  database: 'cowork_complet',
  autoLoadEntities: true,
  synchronize: true,
  logging: true,
}),
```

La base de données ```cowork_complet``` est utilisée pour ne pas entrer en conflit avec vos données existantes, au cas où vous utilisiez déjà une bd ```cowork```.