import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Reservation} from "../models/reservation.model";
import {CreerReservationBureauDto} from "../../site/reservations-bureaux/dto/creer-reservation-bureau.dto";

@Injectable({
  providedIn: 'root'
})
export class ReservationsBureauxService {
  apiUrl = `${environment.apiBaseUrl}`;

  constructor(private httpClient: HttpClient) { }

  obtenirReservation(id: number): Observable<Reservation> {
    const url = `${this.apiUrl}/reservations/${id}`;
    return this.httpClient.get<Reservation>(url);
  }

  obtenirReservationsPourBureau(bureauId: number): Observable<Reservation[]> {
    const url = `${this.apiUrl}/bureaux/${bureauId}/reservations`;
    return this.httpClient.get<Reservation[]>(url);
  }

  creerReservation(bureauId: number, reservation: CreerReservationBureauDto): Observable<Reservation> {
    const url = `${this.apiUrl}/bureaux/${bureauId}/reservations`;
    return this.httpClient.post<Reservation>(url, reservation);
  }
}
