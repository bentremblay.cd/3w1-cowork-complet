import { TestBed } from '@angular/core/testing';

import { BdEnMemoireService } from './bd-en-memoire.service';

describe('BdEnMemoireService', () => {
  let service: BdEnMemoireService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BdEnMemoireService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
