import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Salle} from "../models/salle.model";
import {Observable} from "rxjs";
import {Bureau} from "../models/bureau.model";

@Injectable({
  providedIn: 'root'
})
export class SallesService {
  apiUrl = 'api/salles'

  constructor(private httpClient: HttpClient) { }

  obtenirSalles(): Observable<Salle[]> {
    return this.httpClient.get<Salle[]>(this.apiUrl);
  }

  obtenirSalle(id: number): Observable<Salle> {
    const url = `${this.apiUrl}/${id}`;
    return this.httpClient.get<Salle>(url);
  }

  creerSalle(salle: Salle): Observable<Salle> {
    return this.httpClient.post<Salle>(this.apiUrl, salle);
  }

  modifierSalle(salle: Salle): Observable<any> {
    const url = `${this.apiUrl}/${salle.id}`;
    return this.httpClient.post<any>(url, salle);
  }
}
