import { Injectable } from '@angular/core';
import {Inscription} from "../models/inscription.model";
import {HttpClient} from "@angular/common/http";
import {catchError, map, Observable, of} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class InscriptionService {
  apiUrl = `${environment.apiBaseUrl}/utilisateurs`;

  constructor(private httpClient: HttpClient) { }

  inscrireUtilisateur(inscription: Inscription): Observable<boolean> {
    return this.httpClient.post<{ access_token: string }>(this.apiUrl, inscription).pipe(
      map( reponse => {
        const token = reponse.access_token;
        if(token) {
          localStorage.setItem('access_token', token);
          return true;
        }

        return false;
      }),
      catchError( () => {
        return of(false);
      })
    );
  }
}
