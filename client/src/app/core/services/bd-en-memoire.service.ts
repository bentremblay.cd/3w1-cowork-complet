import { Injectable } from '@angular/core';
import {InMemoryDbService, RequestInfo, ResponseOptions, STATUS} from "angular-in-memory-web-api";
import * as bcrypt from 'bcryptjs';
import {Inscription} from "../models/inscription.model";
import {Utilisateur} from "../models/utilisateur.model";

@Injectable({
  providedIn: 'root'
})
export class BdEnMemoireService implements InMemoryDbService {

  createDb() {
    return {
      bureaux: [
        {
          id: 1,
          code: '01-100',
          capacite: 1,
          prive: false
        },
        {
          id: 2,
          code: '01-101',
          capacite: 1,
          prive: false
        },
        {
          id: 3,
          code: '01-102',
          capacite: 1,
          prive: false
        },
        {
          id: 4,
          code: '01-104',
          capacite: 1,
          prive: false
        },
        {
          id: 5,
          code: '02-100',
          capacite: 1,
          prive: true
        },
        {
          id: 6,
          code: '02-101',
          capacite: 1,
          prive: true
        },
        {
          id: 7,
          code: '02-102',
          capacite: 2,
          prive: true
        }
      ],
      salles: [
        {
          id: 1,
          nom: "Belle salle",
          code: "S01-100",
          etage: 1,
          capacite: 4,
          description:"",
          aEcran: true
        },
        {
          id: 2,
          nom: "Moyenne salle",
          code: "S01-103",
          etage: 1,
          capacite: 12,
          description:"",
          aEcran: true
        },
        {
          id: 3,
          nom: "Petite salle",
          code: "S01-105",
          etage: 1,
          capacite: 2,
          description:"",
          aEcran: false
        },
        {
          id: 3,
          nom: "Grande salle",
          code: "S02-109",
          etage: 2,
          capacite: 24,
          description:"",
          aEcran: true
        },
      ],
      reservations: [
        {
          id: 1,
          bureauId: 2,
          date: '2022-09-29',
          prenom: 'Marc',
          nom: 'Arcand',
          courriel: 'marc.arcand@hello.com',
          adresse: '123',
          codePostal: 'J0C1K0',
          commentaire: ''
        }
      ],
      utilisateurs: [
        {
          id: 1,
          prenom: 'Marc',
          nom: 'Arcand',
          courriel: 'u@ser.com',
          token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxIiwibm9tIjoiQXJjYW5kIiwicHJlbm9tIjoiTWFyYyIsImNvdXJyaWVsIjoidUBzZXIuY29tIiwiaWF0IjoxNTE2MjM5MDIyfQ.H3IUpEsfSzHrViLB-5f6sOcrx0uFSY2iBR1MUFlvHmI',
          // Important: le mot de passe ne doit pas être retourné par l'API!
          motDePasse: bcrypt.hashSync('1234'),
        }
      ]
    }
  }

  post(reqInfo: RequestInfo) {
    /**
     * Si la collection/ressource est 'inscription', nous interceptons la requête pour la faire passer
     * dans notre fonction d'inscription personnalisée.
     */
    if (reqInfo.collectionName === 'inscription')
      return this.inscription(reqInfo);

    /**
     * Si la collection/ressource est 'auth', nous interceptons la requête pour la faire passer dans la fonction
     * d'authentification.
     */
    if (reqInfo.collectionName === 'auth')
      return this.auth(reqInfo);

    /**
     * En retournant undefined, l'exécution normale de la requête POST continue
     */
    return undefined
  }

  auth(reqInfo: RequestInfo) {
    const { headers, url, req } = reqInfo;
    const {courriel, motDePasse} = (<any>req)['body'];
    const currentDb = reqInfo.utils.getDb() as { utilisateurs: Utilisateur[] };

    /* Valeur par défaut de la réponse, soit unauthorized */
    let optionsReponse = {
      status: STATUS.UNAUTHORIZED,
      headers,
      url,
      body: { }
    } as ResponseOptions

    /**
     * Recherche l'utilisateur par courriel
     */
    let utilisateur = currentDb.utilisateurs.find( (utilisateur) => utilisateur.courriel == courriel );

    if(utilisateur && utilisateur.motDePasse && bcrypt.compareSync(motDePasse, utilisateur.motDePasse)) {
      const { motDePasse, ...utilisateurSansMotDePasse } = utilisateur;
      optionsReponse.status = STATUS.OK;
      optionsReponse.body = utilisateurSansMotDePasse
    }

    return reqInfo.utils.createResponse$(() => optionsReponse);
  }

  inscription(reqInfo: RequestInfo) {
    const { headers, url, req } = reqInfo;
    const inscription = ((<any>req))['body'] as Inscription;
    const currentDb = reqInfo.utils.getDb() as { utilisateurs: Utilisateur[] };
    const dernierUtilisateurId = currentDb.utilisateurs.pop()?.id;
    const id = dernierUtilisateurId ? dernierUtilisateurId + 1 : 1;

    const utilisateur = {
      id: id,
      prenom: inscription.prenom,
      nom: inscription.nom,
      courriel: inscription.courriel,
      token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxIiwibm9tIjoiVHJlbWJsYXkiLCJwcmVub20iOiJCZW5vaXQiLCJjb3VycmllbCI6ImJlbm9pdC50cmVtYmxheUBjZWdlcGRydW1tb25kLmNhIiwiaWF0IjoxNTE2MjM5MDIyfQ.W-KKsNj6Rc0YWqF48IXXwulyi4_RaY5_xnZBvSjV5PE',
      motDePasse: bcrypt.hashSync(inscription.motDePasse)
    };

    currentDb.utilisateurs.push(utilisateur);

    const options: ResponseOptions = {
      status: STATUS.OK,
      headers,
      url,
      body: { token: utilisateur.token }
    }

    return reqInfo.utils.createResponse$(() => options);
  }

}
