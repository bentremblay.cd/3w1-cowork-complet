import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Inscription} from "../models/inscription.model";
import {Observable} from "rxjs";
import {Utilisateur} from "../models/utilisateur.model";

@Injectable({
  providedIn: 'root'
})
export class UtilisateursService {
  apiUrl = `${environment.apiBaseUrl}/utilisateurs`;

  constructor(private httpClient: HttpClient) { }

  profil(): Observable<Utilisateur> {
    const url = `${this.apiUrl}/profil`
    return this.httpClient.get<Utilisateur>(url);
  }
}
