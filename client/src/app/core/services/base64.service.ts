import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class Base64Service {

  constructor() { }

  encoderFichier(fichier: File): Observable<string> {
    let resultat = new BehaviorSubject('');
    let fileReader = new FileReader();
    fileReader.readAsDataURL(fichier)
    fileReader.onload = () => {
      if(fileReader.result)
        resultat.next(fileReader.result.toString());
    }

    return resultat;
  }
}
