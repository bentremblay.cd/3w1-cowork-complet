import { Injectable } from '@angular/core';
import {Bureau} from "../models/bureau.model";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";
import {CreerBureauDto} from "../../admin/bureaux/dto/creer-bureau.dto";
import {ModifierBureauDto} from "../../admin/bureaux/dto/modifier-bureau.dto";
import {FiltrerBureauxDto} from "../../admin/bureaux/dto/filtrer-bureaux-dto";

@Injectable({
  providedIn: 'root'
})
export class BureauxService {
  apiUrl = `${environment.apiBaseUrl}/bureaux`;

  constructor(private clientHttp: HttpClient) { }

  obtenirBureaux(): Observable<Bureau[]> {
    return this.clientHttp.get<Bureau[]>(this.apiUrl);
  }

  obtenirBureau(id: number): Observable<Bureau> {
    const url = `${this.apiUrl}/${id}`;
    return this.clientHttp.get<Bureau>(url);
  }

  creerBureau(creerBureauDto: CreerBureauDto): Observable<Bureau> {
    return this.clientHttp.post<Bureau>(this.apiUrl, creerBureauDto);
  }

  modifierBureau(id: number, modifierBureauDto: ModifierBureauDto): Observable<any> {
    const url = `${this.apiUrl}/${id}`;
    return this.clientHttp.patch<any>(url, modifierBureauDto);
  }

  filtrerBureaux(filtrerBureauxDto: FiltrerBureauxDto): Observable<Bureau[]> {
    const url = `${this.apiUrl}/filtrer`;
    return this.clientHttp.post<Bureau[]>(url, filtrerBureauxDto);
  }
}
