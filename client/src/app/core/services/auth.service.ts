import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {catchError, map, Observable, of, throwError} from "rxjs";
import {JwtHelperService} from "@auth0/angular-jwt";
import {Utilisateur} from "../models/utilisateur.model";
import {environment} from "../../../environments/environment";
import {Role} from "../enums/role.enum";
import {InfosConnexionDto} from "../dto/connexion/infos-connexion-dto";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  apiUrl = `${environment.apiBaseUrl}/auth`

  constructor(private httpClient: HttpClient,
              private jwtHelper: JwtHelperService) { }

  connexion(infosUtilisateur: InfosConnexionDto): Observable<boolean> {
    return this.httpClient.post<{ access_token: string }>(this.apiUrl, infosUtilisateur).pipe(
      map( reponse => {
        if(reponse.access_token) {
          localStorage.setItem('access_token', reponse.access_token);
          return true;
        }

        return false;
      }),
      catchError((err) => {
        if(err.status == 401)
          return throwError(() => new Error('La combinaison de courriel et mot de passe ne permet pas de vous identifier.'));

        return throwError(() => new Error('Désolé, impossible de vous identifier.'));
      })
    );
  }

  utilisateur(): Utilisateur | undefined | null {
    const utilisateur = <{sub: number, courriel: string, role: string}>this.jwtHelper.decodeToken();

    if(utilisateur){
      return {
        id: utilisateur.sub,
        courriel: utilisateur.courriel,
        role: utilisateur.role as Role,
      };
    }

    return undefined;
  }

  deconnexion() {
    localStorage.removeItem('access_token');
  }

  estConnecte() {
    return this.jwtHelper.tokenGetter() !== null && !this.jwtHelper.isTokenExpired();
  }
}
