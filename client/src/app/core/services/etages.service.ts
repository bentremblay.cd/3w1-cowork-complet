import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Etage} from "../models/etage.model";
import {CreerEtageDto} from "../../admin/etages/dto/creer-etage.dto";
import {ModifierEtageDto} from "../../admin/etages/dto/modifier-etage.dto";

@Injectable({
  providedIn: 'root'
})
export class EtagesService {
  apiUrl = `${environment.apiBaseUrl}/etages`;

  constructor(private clientHttp: HttpClient) { }

  obtenirEtages(): Observable<Etage[]> {
    return this.clientHttp.get<Etage[]>(this.apiUrl);
  }

  obtenirEtage(id: number): Observable<Etage> {
    const url = `${this.apiUrl}/${id}`;
    return this.clientHttp.get<Etage>(url);
  }

  creerEtage(creerEtageDto: CreerEtageDto): Observable<Etage> {
    return this.clientHttp.post<Etage>(this.apiUrl, creerEtageDto);
  }

  modifierEtage(id: number, modifierEtageDto: ModifierEtageDto): Observable<any> {
    const url = `${this.apiUrl}/${id}`;
    return this.clientHttp.patch<any>(url, modifierEtageDto);
  }
}
