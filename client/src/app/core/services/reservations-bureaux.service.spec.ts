import { TestBed } from '@angular/core/testing';

import { ReservationsBureauxService } from './reservations-bureaux.service';

describe('ReservationsBureauxService', () => {
  let service: ReservationsBureauxService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReservationsBureauxService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
