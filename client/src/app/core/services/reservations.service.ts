import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Reservation} from "../models/reservation.model";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ReservationsService {
  apiUrl = `${environment.apiBaseUrl}/reservations`;

  constructor(private httpClient: HttpClient) { }

  obtenirReservations(): Observable<Reservation[]> {
    return this.httpClient.get<Reservation[]>(this.apiUrl);
  }

  obtenirReservation(id: number): Observable<Reservation> {
    const url = `${this.apiUrl}/${id}`;
    return this.httpClient.get<Reservation>(url);
  }

  supprimerReservation(id: number): Observable<any> {
    const url = `${this.apiUrl}/${id}`;
    return this.httpClient.delete(url);
  }

  obtenirMesReservations(): Observable<Reservation[]> {
    const url = `${this.apiUrl}/mes-reservations`;
    return this.httpClient.get<Reservation[]>(url);
  }
}
