import {Bureau} from "./bureau.model";
import {Utilisateur} from "./utilisateur.model";

export interface Reservation {
  id: number;
  date: string;
  prenom: string;
  nom: string;
  courriel: string;
  adresse: string;
  ville: string;
  codePostal: string;
  commentaire: string;
  bureau: Bureau;
  utilisateur?: Utilisateur;
}
