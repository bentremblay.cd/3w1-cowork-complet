import {Etage} from "./etage.model";

export interface Bureau {
  id: number;
  code: string;
  etage: Etage;
  capacite: number;
  estPrive: boolean;
  description?: string;
  image?: string;
}
