export interface Salle {
  id?: number;
  nom: string;
  code: string;
  etage: number;
  capacite: number;
  description?: string;
  image?: string;
  aEcran: boolean;
}
