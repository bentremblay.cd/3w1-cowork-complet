export interface Inscription {
  prenom: string;
  nom: string;
  courriel: string;
  motDePasse: string;
}

