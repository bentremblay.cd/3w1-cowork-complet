import {Role} from "../enums/role.enum";

export interface Utilisateur {
  id?: number;
  courriel: string;
  nom?: string;
  prenom?: string;
  role: Role;
}
