import {AbstractControl, ValidationErrors, ValidatorFn} from "@angular/forms";

export const confirmationMotDePasseValidator: ValidatorFn = (group: AbstractControl): ValidationErrors | null => {
  const motDePasse = group.get('motDePasse');
  const confirmationMotDePasse = group.get('confirmationMotDePasse');

  if(motDePasse?.pristine || confirmationMotDePasse?.pristine)
    return null;

  if(motDePasse?.value === confirmationMotDePasse?.value)
    return null;

  return { confirmationMotDePasse: true };
}

