import { Component, OnInit } from '@angular/core';
import {Reservation} from "../../../core/models/reservation.model";
import {ReservationsService} from "../../../core/services/reservations.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-reservation-details',
  templateUrl: './reservation-details.component.html',
  styleUrls: ['./reservation-details.component.scss']
})
export class ReservationDetailsComponent implements OnInit {
  reservation!: Reservation;

  constructor(
    private reservationsService: ReservationsService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    const reservationId = Number(this.route.snapshot.paramMap.get('id'));
    this.reservationsService.obtenirReservation(reservationId).subscribe((reservation) => {
      this.reservation = reservation;
    });
  }

  annuler() {
    if(this.reservation?.id &&
      confirm(`Êtes-vous certain de vouloir annuler votre réservation? Cette action est irréversible.`)) {
      this.reservationsService.supprimerReservation(this.reservation.id).subscribe((reservation) => {
        this.router.navigate(['/admin', 'reservations'])
      })
    }
  }
}
