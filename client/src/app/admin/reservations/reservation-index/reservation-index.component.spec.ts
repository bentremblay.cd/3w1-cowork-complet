import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservationIndexComponent } from './reservation-index.component';

describe('ReservationIndexComponent', () => {
  let component: ReservationIndexComponent;
  let fixture: ComponentFixture<ReservationIndexComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReservationIndexComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReservationIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
