import {Component, Input, OnInit} from '@angular/core';
import {Reservation} from "../../../core/models/reservation.model";
import {ReservationsService} from "../../../core/services/reservations.service";
import {Bureau} from "../../../core/models/bureau.model";

@Component({
  selector: '[app-reservation-item]',
  templateUrl: './reservation-item.component.html',
  styleUrls: ['./reservation-item.component.scss']
})
export class ReservationItemComponent implements OnInit {
  @Input() reservation!: Reservation;

  constructor() { }

  ngOnInit(): void {
  }

}
