import { Component, OnInit } from '@angular/core';
import {Reservation} from "../../../core/models/reservation.model";
import {ReservationsService} from "../../../core/services/reservations.service";

@Component({
  selector: 'app-reservation-list',
  templateUrl: './reservation-list.component.html',
  styleUrls: ['./reservation-list.component.scss']
})
export class ReservationListComponent implements OnInit {
  reservations: Reservation[] = []

  constructor(private reservationsService: ReservationsService) { }

  ngOnInit(): void {
    this.reservationsService.obtenirReservations().subscribe((reservations) => {
      this.reservations = reservations;
    })
  }

}
