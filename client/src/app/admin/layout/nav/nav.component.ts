import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../../core/services/auth.service";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  get utilisateur() {
    return this.authService.utilisateur();
  }

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }

  deconnexion() {
    this.authService.deconnexion();
  }

}
