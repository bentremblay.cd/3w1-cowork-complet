import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AdminComponent} from "./admin.component";
import {BureauIndexComponent} from "./bureaux/bureau-index/bureau-index.component";
import {BureauCreerComponent} from "./bureaux/bureau-creer/bureau-creer.component";
import {BureauModifierComponent} from "./bureaux/bureau-modifier/bureau-modifier.component";
import {ConnexionComponent} from "./connexion/connexion.component";
import {AuthGuard} from "../core/guards/auth.guard";
import {ReservationIndexComponent} from "./reservations/reservation-index/reservation-index.component";
import {ReservationDetailsComponent} from "./reservations/reservation-details/reservation-details.component";
import {EtageIndexComponent} from "./etages/etage-index/etage-index.component";
import {EtageCreerComponent} from "./etages/etage-creer/etage-creer.component";
import {EtageModifierComponent} from "./etages/etage-modifier/etage-modifier.component";

const routes: Routes = [
  {
    path: '', component: AdminComponent, canActivateChild: [AuthGuard], children: [
      {
        path: '', component: BureauIndexComponent
      },
      {
        path: 'bureaux/nouveau', component: BureauCreerComponent
      },
      {
        path: 'bureaux/:id/modifier', component: BureauModifierComponent
      },
      {
        path: 'reservations', component: ReservationIndexComponent
      },
      {
        path: 'reservations/:id', component: ReservationDetailsComponent
      },
      {
        path: 'etages', component: EtageIndexComponent
      },
      {
        path: 'etages/nouveau', component: EtageCreerComponent
      },
      {
        path: 'etages/:id/modifier', component: EtageModifierComponent
      },
    ]
  },
  {
    path: '', component: AdminComponent, children : [
      {
        path: 'connexion', component: ConnexionComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
