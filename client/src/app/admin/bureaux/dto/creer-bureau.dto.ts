export interface CreerBureauDto {
  code: string;
  etageId: number;
  capacite: number;
  estPrive: boolean;
  description?: string;
  image?: string;
}
