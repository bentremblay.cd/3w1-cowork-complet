import {CreerBureauDto} from "./creer-bureau.dto";

export interface ModifierBureauDto extends Partial<CreerBureauDto> {}
