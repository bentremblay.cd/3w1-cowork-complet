import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BureauModifierComponent } from './bureau-modifier.component';

describe('BureauModifierComponent', () => {
  let component: BureauModifierComponent;
  let fixture: ComponentFixture<BureauModifierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BureauModifierComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BureauModifierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
