import { Component, OnInit } from '@angular/core';
import {Bureau} from "../../../core/models/bureau.model";
import {FormBuilder, Validators} from "@angular/forms";
import {BureauxService} from "../../../core/services/bureaux.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Base64Service} from "../../../core/services/base64.service";
import {ModifierBureauDto} from "../dto/modifier-bureau.dto";
import {EtagesService} from "../../../core/services/etages.service";
import {Etage} from "../../../core/models/etage.model";

@Component({
  selector: 'app-bureau-modifier',
  templateUrl: './bureau-modifier.component.html',
  styleUrls: ['./bureau-modifier.component.scss']
})
export class BureauModifierComponent implements OnInit {
  bureau!: Bureau;
  etages: Etage[] = [];
  formulaire = this.fb.group({
    code: ['', [
      Validators.required,
      Validators.pattern(new RegExp('^[0-9]{2}-[0-9]{3}$'))]
    ],
    etageId: [0, [Validators.required]],
    capacite: [1, [Validators.required]],
    estPrive: [false, [Validators.required]],
    description: [''],
    image: [''],
  }, {
    updateOn: "submit"
  });

  get code() {
    return this.formulaire.get('code');
  }

  get etageId() {
    return this.formulaire.get('etageId');
  }

  get capacite() {
    return this.formulaire.get('capacite');
  }

  get estPrive() {
    return this.formulaire.get('estPrive');
  }

  get description() {
    return this.formulaire.get('description');
  }

  get image() {
    return this.formulaire.get('image');
  }

  constructor(private fb: FormBuilder,
              private bureauxService: BureauxService,
              private etagesService: EtagesService,
              private router: Router,
              private route: ActivatedRoute,
              private base64Service: Base64Service) { }

  ngOnInit(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.bureauxService.obtenirBureau(id).subscribe((bureau) => {
      this.bureau = bureau;
      this.formulaire.patchValue(this.bureau);
      this.formulaire.patchValue({etageId: this.bureau.etage.id})
    });
    this.obtenirEtages();
  }

  obtenirEtages() {
    this.etagesService.obtenirEtages().subscribe((etages) => {
      this.etages = etages;
    })
  }

  encodeImage(event: Event) {
    let inputElement = event.target as HTMLInputElement;
    if(inputElement?.files?.length) {
      let fichier = inputElement.files[0];
      this.base64Service.encoderFichier(fichier).subscribe((fichierBase64) => {
        this.formulaire.patchValue({
          image: fichierBase64
        })
      })
    }
  }

  soumettre() {
    if(this.formulaire.valid) {
      this.bureauxService.modifierBureau(this.bureau.id, this.formulaire.value as ModifierBureauDto).subscribe((bureau) => {
        this.router.navigate(['/admin']);
      })
    }
  }

}
