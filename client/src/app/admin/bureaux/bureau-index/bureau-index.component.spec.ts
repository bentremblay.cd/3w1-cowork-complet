import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BureauIndexComponent } from './bureau-index.component';

describe('BureauIndexComponent', () => {
  let component: BureauIndexComponent;
  let fixture: ComponentFixture<BureauIndexComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BureauIndexComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BureauIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
