import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BureauCreerComponent } from './bureau-creer.component';

describe('BureauCreerComponent', () => {
  let component: BureauCreerComponent;
  let fixture: ComponentFixture<BureauCreerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BureauCreerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BureauCreerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
