import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {BureauxService} from "../../../core/services/bureaux.service";
import {Router} from "@angular/router";
import {Base64Service} from "../../../core/services/base64.service";
import {EtagesService} from "../../../core/services/etages.service";
import {Etage} from "../../../core/models/etage.model";
import {CreerBureauDto} from "../dto/creer-bureau.dto";

@Component({
  selector: 'app-bureau-creer',
  templateUrl: './bureau-creer.component.html',
  styleUrls: ['./bureau-creer.component.scss']
})
export class BureauCreerComponent implements OnInit {
  etages: Etage[] = [];
  formulaire = this.fb.group({
    code: ['', [
      Validators.required,
      Validators.pattern(new RegExp('^[0-9]{2}-[0-9]{3}$'))]
    ],
    etageId: [0, [Validators.required]],
    capacite: [1, [Validators.required]],
    estPrive: [false, [Validators.required]],
    description: [''],
    image: [''],
  }, {
    updateOn: "submit"
  });

  get code() {
    return this.formulaire.get('code');
  }

  get etageId() {
    return this.formulaire.get('etageId');
  }

  get capacite() {
    return this.formulaire.get('capacite');
  }

  get estPrive() {
    return this.formulaire.get('prive');
  }

  get description() {
    return this.formulaire.get('description');
  }

  get image() {
    return this.formulaire.get('image');
  }

  constructor(private fb: FormBuilder,
              private bureauxService: BureauxService,
              private etagesService: EtagesService,
              private router: Router,
              private base64Service: Base64Service) { }

  ngOnInit(): void {
    this.obtenirEtages();
  }

  obtenirEtages() {
    this.etagesService.obtenirEtages().subscribe((etages) => {
      this.etages = etages;
    })
  }

  encodeImage(event: Event) {
    let inputElement = event.target as HTMLInputElement;
    if(inputElement?.files?.length) {
      let fichier = inputElement.files[0];
      this.base64Service.encoderFichier(fichier).subscribe((fichierBase64) => {
        this.formulaire.patchValue({
          image: fichierBase64
        })
      })
    }
  }

  soumettre() {
    if(this.formulaire.valid) {
      this.bureauxService.creerBureau(this.formulaire.value as CreerBureauDto).subscribe((bureau) => {
        this.router.navigate(['/admin']);
      })
    }
  }
}
