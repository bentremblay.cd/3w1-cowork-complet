import {Component, Input, OnInit} from '@angular/core';
import {Bureau} from "../../../core/models/bureau.model";

@Component({
  selector: '[app-bureau-item]',
  templateUrl: './bureau-item.component.html',
  styleUrls: ['./bureau-item.component.scss']
})
export class BureauItemComponent implements OnInit {
  @Input() bureau!: Bureau;

  constructor() { }

  ngOnInit(): void {
  }

}
