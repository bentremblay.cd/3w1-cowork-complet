import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BureauItemComponent } from './bureau-item.component';

describe('BureauItemComponent', () => {
  let component: BureauItemComponent;
  let fixture: ComponentFixture<BureauItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BureauItemComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BureauItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
