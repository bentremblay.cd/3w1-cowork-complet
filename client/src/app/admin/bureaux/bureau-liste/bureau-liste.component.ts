import { Component, OnInit } from '@angular/core';
import {Bureau} from "../../../core/models/bureau.model";
import {BureauxService} from "../../../core/services/bureaux.service";
import {FormBuilder, Validators} from "@angular/forms";

@Component({
  selector: 'app-bureau-liste',
  templateUrl: './bureau-liste.component.html',
  styleUrls: ['./bureau-liste.component.scss']
})
export class BureauListeComponent implements OnInit {
  bureaux: Bureau[] = [];

  constructor(private bureauxService: BureauxService,
              private fb: FormBuilder) { }

  ngOnInit(): void {
    this.bureauxService.obtenirBureaux().subscribe((bureaux) => {
      this.bureaux = bureaux;
    })
  }
}
