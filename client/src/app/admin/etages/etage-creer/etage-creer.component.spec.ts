import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EtageCreerComponent } from './etage-creer.component';

describe('EtageCreerComponent', () => {
  let component: EtageCreerComponent;
  let fixture: ComponentFixture<EtageCreerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EtageCreerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EtageCreerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
