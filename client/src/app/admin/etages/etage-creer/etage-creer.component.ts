import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {EtagesService} from "../../../core/services/etages.service";
import {CreerEtageDto} from "../dto/creer-etage.dto";
import {Router} from "@angular/router";

@Component({
  selector: 'app-etage-creer',
  templateUrl: './etage-creer.component.html',
  styleUrls: ['./etage-creer.component.scss']
})
export class EtageCreerComponent implements OnInit {
  formulaire = this.fb.group({
    nom: ['', [Validators.required]],
  }, {
    updateOn: "submit"
  });

  get nom() {
    return this.formulaire.get('nom');
  }

  constructor(private fb: FormBuilder,
              private etagesService: EtagesService,
              private router: Router) { }

  ngOnInit(): void {
  }

  soumettre() {
    if(this.formulaire.valid) {
      this.etagesService.creerEtage(this.formulaire.value as CreerEtageDto).subscribe((etage) => {
        this.router.navigate(['/admin', 'etages']);
      })
    }
  }

}
