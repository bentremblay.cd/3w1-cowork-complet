import { Component, OnInit } from '@angular/core';
import {Etage} from "../../../core/models/etage.model";
import {EtagesService} from "../../../core/services/etages.service";

@Component({
  selector: 'app-etage-list',
  templateUrl: './etage-list.component.html',
  styleUrls: ['./etage-list.component.scss']
})
export class EtageListComponent implements OnInit {
  etages: Etage[] = [];

  constructor(private etagesService: EtagesService) { }

  ngOnInit(): void {
    this.etagesService.obtenirEtages().subscribe((etages) => {
      this.etages = etages;
    })
  }

}
