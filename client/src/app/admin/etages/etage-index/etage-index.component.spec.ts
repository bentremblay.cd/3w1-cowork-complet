import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EtageIndexComponent } from './etage-index.component';

describe('EtageIndexComponent', () => {
  let component: EtageIndexComponent;
  let fixture: ComponentFixture<EtageIndexComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EtageIndexComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EtageIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
