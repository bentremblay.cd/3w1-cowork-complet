import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EtageModifierComponent } from './etage-modifier.component';

describe('EtageModifierComponent', () => {
  let component: EtageModifierComponent;
  let fixture: ComponentFixture<EtageModifierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EtageModifierComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EtageModifierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
