import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {EtagesService} from "../../../core/services/etages.service";
import {ActivatedRoute, Router} from "@angular/router";
import {CreerEtageDto} from "../dto/creer-etage.dto";
import {ModifierEtageDto} from "../dto/modifier-etage.dto";
import {Etage} from "../../../core/models/etage.model";

@Component({
  selector: 'app-etage-modifier',
  templateUrl: './etage-modifier.component.html',
  styleUrls: ['./etage-modifier.component.scss']
})
export class EtageModifierComponent implements OnInit {
  etage!: Etage;
  formulaire = this.fb.group({
    nom: ['', [Validators.required]],
  }, {
    updateOn: "submit"
  });

  get nom() {
    return this.formulaire.get('nom');
  }

  constructor(private fb: FormBuilder,
              private etagesService: EtagesService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
    const etageId = Number(this.route.snapshot.paramMap.get('id'));
    this.etagesService.obtenirEtage(etageId).subscribe((etage)=> {
      this.etage = etage;
      this.formulaire.patchValue(etage);
    });
  }

  soumettre() {
    if(this.formulaire.valid) {
      this.etagesService.modifierEtage(this.etage.id, this.formulaire.value as ModifierEtageDto).subscribe((etage) => {
        this.router.navigate(['/admin', 'etages']);
      })
    }
  }
}
