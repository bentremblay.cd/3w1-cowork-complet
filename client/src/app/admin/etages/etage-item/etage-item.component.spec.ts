import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EtageItemComponent } from './etage-item.component';

describe('EtageItemComponent', () => {
  let component: EtageItemComponent;
  let fixture: ComponentFixture<EtageItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EtageItemComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EtageItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
