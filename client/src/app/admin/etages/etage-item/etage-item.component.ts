import {Component, Input, OnInit} from '@angular/core';
import {Etage} from "../../../core/models/etage.model";
import {EtagesService} from "../../../core/services/etages.service";

@Component({
  selector: '[app-etage-item]',
  templateUrl: './etage-item.component.html',
  styleUrls: ['./etage-item.component.scss']
})
export class EtageItemComponent implements OnInit {
  @Input() etage!: Etage;

  constructor(private etagesService: EtagesService) { }

  ngOnInit(): void {
  }
}
