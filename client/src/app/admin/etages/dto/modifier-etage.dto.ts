import {CreerEtageDto} from "./creer-etage.dto";

export interface ModifierEtageDto extends Partial<CreerEtageDto> {}
