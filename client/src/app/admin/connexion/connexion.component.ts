import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {AuthService} from "../../core/services/auth.service";
import {InfosConnexionDto} from "../../core/dto/connexion/infos-connexion-dto";

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.scss']
})
export class ConnexionComponent implements OnInit {
  erreurMsg!: string;
  formulaire = this.fb.group({
    courriel: ['', [Validators.required, Validators.email]],
    motDePasse: ['', [Validators.required]],
  }, {
    updateOn: 'submit'
  })

  get courriel() {
    return this.formulaire.get('courriel');
  }

  get motDePasse() {
    return this.formulaire.get('motDePasse');
  }

  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private router: Router) { }

  ngOnInit(): void {
  }

  connexion() {
    if(this.formulaire.valid) {
      this.authService.connexion(this.formulaire.value as InfosConnexionDto).subscribe((connexionReussie) => {
          if(connexionReussie)
            this.router.navigate(['/admin']);
        },
        err => {
          this.erreurMsg = err.message;
        })
    }
  }
}
