import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { NavComponent } from './layout/nav/nav.component';
import { FooterComponent } from './layout/footer/footer.component';
import { BureauIndexComponent } from './bureaux/bureau-index/bureau-index.component';
import { BureauListeComponent } from './bureaux/bureau-liste/bureau-liste.component';
import { BureauItemComponent } from './bureaux/bureau-item/bureau-item.component';
import { BureauCreerComponent } from './bureaux/bureau-creer/bureau-creer.component';
import {ReactiveFormsModule} from "@angular/forms";
import { BureauModifierComponent } from './bureaux/bureau-modifier/bureau-modifier.component';
import { ConnexionComponent } from './connexion/connexion.component';
import { ReservationIndexComponent } from './reservations/reservation-index/reservation-index.component';
import { ReservationListComponent } from './reservations/reservation-list/reservation-list.component';
import { ReservationItemComponent } from './reservations/reservation-item/reservation-item.component';
import { ReservationDetailsComponent } from './reservations/reservation-details/reservation-details.component';
import { EtageIndexComponent } from './etages/etage-index/etage-index.component';
import { EtageCreerComponent } from './etages/etage-creer/etage-creer.component';
import { EtageModifierComponent } from './etages/etage-modifier/etage-modifier.component';
import { EtageListComponent } from './etages/etage-list/etage-list.component';
import { EtageItemComponent } from './etages/etage-item/etage-item.component';


@NgModule({
  declarations: [
    AdminComponent,
    NavComponent,
    FooterComponent,
    BureauIndexComponent,
    BureauListeComponent,
    BureauItemComponent,
    BureauCreerComponent,
    BureauModifierComponent,
    ConnexionComponent,
    ReservationIndexComponent,
    ReservationListComponent,
    ReservationItemComponent,
    ReservationDetailsComponent,
    EtageIndexComponent,
    EtageCreerComponent,
    EtageModifierComponent,
    EtageListComponent,
    EtageItemComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    ReactiveFormsModule
  ]
})
export class AdminModule { }
