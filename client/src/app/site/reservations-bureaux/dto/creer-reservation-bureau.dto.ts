export interface CreerReservationBureauDto {
  date: string;
  prenom: string;
  nom: string;
  courriel: string;
  adresse: string;
  ville: string;
  codePostal: string;
  commentaire: string;
}
