import { Component, OnInit } from '@angular/core';
import {ReservationsService} from "../../../core/services/reservations.service";
import {Reservation} from "../../../core/models/reservation.model";
import {ActivatedRoute} from "@angular/router";
import {ReservationsBureauxService} from "../../../core/services/reservations-bureaux.service";

@Component({
  selector: 'app-reservation-bureau-confirmation',
  templateUrl: './reservation-bureau-confirmation.component.html',
  styleUrls: ['./reservation-bureau-confirmation.component.scss']
})
export class ReservationBureauConfirmationComponent implements OnInit {
  reservation!: Reservation;

  constructor(private reservationsService: ReservationsBureauxService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.reservationsService.obtenirReservation(id).subscribe((reservation) => {
      this.reservation = reservation;
    })
  }

}
