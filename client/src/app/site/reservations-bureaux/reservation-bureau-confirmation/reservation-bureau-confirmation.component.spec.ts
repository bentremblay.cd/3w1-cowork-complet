import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservationBureauConfirmationComponent } from './reservation-bureau-confirmation.component';

describe('ReservationBureauConfirmationComponent', () => {
  let component: ReservationBureauConfirmationComponent;
  let fixture: ComponentFixture<ReservationBureauConfirmationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReservationBureauConfirmationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReservationBureauConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
