import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {Bureau} from "../../../core/models/bureau.model";
import {BureauxService} from "../../../core/services/bureaux.service";
import {ReservationsBureauxService} from "../../../core/services/reservations-bureaux.service";
import {CreerReservationBureauDto} from "../dto/creer-reservation-bureau.dto";
import {UtilisateursService} from "../../../core/services/utilisateurs.service";
import {AuthService} from "../../../core/services/auth.service";

@Component({
  selector: 'app-reservation-bureau-creer',
  templateUrl: './reservation-bureau-creer.component.html',
  styleUrls: ['./reservation-bureau-creer.component.scss']
})
export class ReservationBureauCreerComponent implements OnInit {
  date!: string | null;
  bureau!: Bureau;

  formulaire = this.fb.group({
    prenom: ['', Validators.required],
    nom: ['', Validators.required],
    courriel: ['', [Validators.required, Validators.email]],
    adresse: ['', Validators.required],
    ville: ['', Validators.required],
    codePostal: ['', Validators.required],
    commentaire: [''],
  }, {
    updateOn: 'submit'
  })

  get prenom() {
    return this.formulaire.get('prenom');
  }

  get nom() {
    return this.formulaire.get('nom');
  }

  get courriel() {
    return this.formulaire.get('courriel');
  }

  get adresse() {
    return this.formulaire.get('adresse');
  }

  get ville() {
    return this.formulaire.get('adresse');
  }

  get codePostal() {
    return this.formulaire.get('codePostal');
  }

  get commentaire() {
    return this.formulaire.get('codePostal');
  }

  constructor(private bureauService: BureauxService,
              private reservationsService: ReservationsBureauxService,
              private utilisateursService: UtilisateursService,
              private authService: AuthService,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.bureauService.obtenirBureau(id).subscribe((bureau) => {
      this.bureau = bureau;
    })

    this.verifierPresenceDate();
    this.initialiserFormulaire();
  }

  initialiserFormulaire() {
    if(this.authService.estConnecte()) {
      this.utilisateursService.profil().subscribe((utilisateur) => {
        const { prenom, nom, courriel } = utilisateur;
        this.formulaire.patchValue({
          nom: nom,
          prenom: prenom,
          courriel: courriel,
        })
      })
    }
  }

  verifierPresenceDate() {
    this.date = this.route.snapshot.queryParamMap.get('date');
    if(this.date == null)
      this.router.navigate(['/bureaux', this.bureau.id]);
  }

  soumettre() {
    if(this.formulaire.valid) {
      let reservation = this.formulaire.value as CreerReservationBureauDto;
      Object.assign(reservation, {
        date: this.date,
      })
      this.reservationsService.creerReservation(this.bureau.id, reservation).subscribe((reservation) => {
        this.router.navigate(['reservations', reservation.id, 'confirmation'])
      });
    }
  }
}
