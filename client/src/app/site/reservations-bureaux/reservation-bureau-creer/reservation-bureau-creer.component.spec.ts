import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservationBureauCreerComponent } from './reservation-bureau-creer.component';

describe('ReservationBureauCreerComponent', () => {
  let component: ReservationBureauCreerComponent;
  let fixture: ComponentFixture<ReservationBureauCreerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReservationBureauCreerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReservationBureauCreerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
