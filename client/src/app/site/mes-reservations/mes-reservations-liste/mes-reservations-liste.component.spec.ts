import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MesReservationsListeComponent } from './mes-reservations-liste.component';

describe('MesReservationsListeComponent', () => {
  let component: MesReservationsListeComponent;
  let fixture: ComponentFixture<MesReservationsListeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MesReservationsListeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MesReservationsListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
