import {Component, OnInit, ViewChild} from '@angular/core';
import {Reservation} from "../../../core/models/reservation.model";
import {ReservationsService} from "../../../core/services/reservations.service";

@Component({
  selector: 'app-mes-reservations-liste',
  templateUrl: './mes-reservations-liste.component.html',
  styleUrls: ['./mes-reservations-liste.component.scss']
})
export class MesReservationsListeComponent implements OnInit {
  reservations!: Reservation[];

  constructor(private reservationsService: ReservationsService) { }

  ngOnInit(): void {
    this.initialiserListe()
  }

  initialiserListe() {
    this.reservationsService.obtenirMesReservations().subscribe((reservations) => {
      this.reservations = reservations;
    })
  }

  confirmationAnnulation(reservation: Reservation) {
    if(confirm(`Êtes-vous certain de vouloir annuler votre réservation? Cette action est irréversible.`)) {
      this.reservationsService.supprimerReservation(reservation.id).subscribe(() => {
        this.initialiserListe();
      });
    }
  }
}
