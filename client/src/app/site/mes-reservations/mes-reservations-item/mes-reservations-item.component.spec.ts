import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MesReservationsItemComponent } from './mes-reservations-item.component';

describe('MesReservationsItemComponent', () => {
  let component: MesReservationsItemComponent;
  let fixture: ComponentFixture<MesReservationsItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MesReservationsItemComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MesReservationsItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
