import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Reservation} from "../../../core/models/reservation.model";
import {formatDate} from "@angular/common";
import {ReservationsService} from "../../../core/services/reservations.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-mes-reservations-item',
  templateUrl: './mes-reservations-item.component.html',
  styleUrls: ['./mes-reservations-item.component.scss']
})
export class MesReservationsItemComponent implements OnInit {
  @Input() reservation!: Reservation;
  @Output() annulerReservationEvenement = new EventEmitter<Reservation>();

  constructor(
    private reservationsService: ReservationsService,
    private router: Router) { }

  ngOnInit(): void {
  }

  formatterDate(date: string): string {
    return formatDate(date, 'yyyy-MM-dd', 'en-CA')
  }

  reservationPassee(): boolean {
    return new Date(this.reservation.date) < new Date();
  }

  annuler() {
    this.annulerReservationEvenement.emit(this.reservation);
  }
}
