import { Component, OnInit } from '@angular/core';
import {Reservation} from "../../core/models/reservation.model";
import {ReservationsService} from "../../core/services/reservations.service";
import {formatDate} from "@angular/common";

@Component({
  selector: 'app-mes-reservations',
  templateUrl: './mes-reservations.component.html',
  styleUrls: ['./mes-reservations.component.scss']
})
export class MesReservationsComponent implements OnInit {
  reservations!: Reservation[];

  constructor(private reservationsService: ReservationsService) { }

  ngOnInit(): void {
    this.reservationsService.obtenirMesReservations().subscribe((reservations) => {
      this.reservations = reservations;
    })
  }

  formatDate(date: string): string {
    return formatDate(date, 'yyyy-MM-dd', 'en-CA')
  }
}
