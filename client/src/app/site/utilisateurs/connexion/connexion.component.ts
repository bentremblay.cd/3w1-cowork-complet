import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {confirmationMotDePasseValidator} from "../../../core/validators/confirmation-mot-de-passe-validator.directive";
import {InscriptionService} from "../../../core/services/inscription.service";
import {Router} from "@angular/router";
import {Inscription} from "../../../core/models/inscription.model";
import {AuthService} from "../../../core/services/auth.service";
import {InfosUtilisateurConnexion} from "../../../core/models/infos-utilisateur-connexion.model";

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.scss']
})
export class ConnexionComponent implements OnInit {
  erreurMsg!: string;
  formulaire = this.fb.group({
    courriel: ['', [Validators.required, Validators.email]],
    motDePasse: ['', [Validators.required]],
  }, {
    updateOn: 'submit'
  })

  get courriel() {
    return this.formulaire.get('courriel');
  }

  get motDePasse() {
    return this.formulaire.get('motDePasse');
  }

  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private router: Router) { }

  ngOnInit(): void {
  }

  connexion() {
    if(this.formulaire.valid) {
      this.authService.connexion(this.formulaire.value as InfosUtilisateurConnexion).subscribe((connexionReussie) => {
        if(connexionReussie)
          this.router.navigate(['/']);
      },
      err => {
        this.erreurMsg = err.message;
      })
    }
  }
}
