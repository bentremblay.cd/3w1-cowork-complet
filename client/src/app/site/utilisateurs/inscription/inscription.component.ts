import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {InscriptionService} from "../../../core/services/inscription.service";
import {Inscription} from "../../../core/models/inscription.model";
import {Router} from "@angular/router";
import {confirmationMotDePasseValidator} from "../../../core/validators/confirmation-mot-de-passe-validator.directive";

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.scss']
})
export class InscriptionComponent implements OnInit {
  formulaire = this.fb.group({
    prenom: ['', [Validators.required]],
    nom: ['', [Validators.required]],
    courriel: ['', [Validators.required, Validators.email]],
    motDePasse: ['', [Validators.required, Validators.minLength(8)]],
    confirmationMotDePasse: ['', [Validators.required, Validators.minLength(8)]],
  }, {
    updateOn: 'submit',
    validators: confirmationMotDePasseValidator
  })

  get prenom() {
    return this.formulaire.get('prenom');
  }

  get nom() {
    return this.formulaire.get('nom');
  }

  get courriel() {
    return this.formulaire.get('courriel');
  }

  get motDePasse() {
    return this.formulaire.get('motDePasse');
  }

  get confirmationMotDePasse() {
    return this.formulaire.get('confirmationMotDePasse');
  }

  constructor(private fb: FormBuilder,
              private inscriptionService: InscriptionService,
              private router: Router) { }

  ngOnInit(): void {
  }

  inscription() {
    if(this.formulaire.valid) {
      this.inscriptionService.inscrireUtilisateur(this.formulaire.value as Inscription).subscribe((inscriptionReussie) => {
        if(inscriptionReussie)
          this.router.navigate(['/']);
      });
    }
  }

}
