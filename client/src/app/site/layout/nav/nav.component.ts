import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../../core/services/auth.service";
import {Router} from "@angular/router";
import {Role} from "../../../core/enums/role.enum";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  constructor(private authService: AuthService,
              private router: Router) { }

  ngOnInit(): void {
  }

  utilisateur() {
    return this.authService.utilisateur();
  }

  estAdmin() {
    return this.utilisateur() && this.utilisateur()?.role == Role.Admin;
  }

  deconnexion() {
    this.authService.deconnexion();
    this.router.navigate(['/']);
  }
}
