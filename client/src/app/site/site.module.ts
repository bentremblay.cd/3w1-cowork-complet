import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SiteRoutingModule } from './site-routing.module';
import {BureauListeComponent} from "./bureaux/bureau-liste/bureau-liste.component";
import { BureauIndexComponent } from './bureaux/bureau-index/bureau-index.component';
import { SiteComponent } from './site.component';
import { NavComponent } from './layout/nav/nav.component';
import { FooterComponent } from './layout/footer/footer.component';
import { BureauItemComponent } from './bureaux/bureau-item/bureau-item.component';
import { BureauDetailsComponent } from './bureaux/bureau-details/bureau-details.component';
import {ReactiveFormsModule} from "@angular/forms";
import { ReservationBureauCreerComponent } from './reservations-bureaux/reservation-bureau-creer/reservation-bureau-creer.component';
import { ReservationBureauConfirmationComponent } from './reservations-bureaux/reservation-bureau-confirmation/reservation-bureau-confirmation.component';
import { InscriptionComponent } from './utilisateurs/inscription/inscription.component';
import { ConnexionComponent } from './utilisateurs/connexion/connexion.component';
import { MesReservationsComponent } from './mes-reservations/mes-reservations.component';
import { MesReservationsListeComponent } from './mes-reservations/mes-reservations-liste/mes-reservations-liste.component';
import { MesReservationsItemComponent } from './mes-reservations/mes-reservations-item/mes-reservations-item.component';


@NgModule({
  declarations: [
    BureauListeComponent,
    BureauIndexComponent,
    SiteComponent,
    NavComponent,
    FooterComponent,
    BureauItemComponent,
    BureauDetailsComponent,
    ReservationBureauCreerComponent,
    ReservationBureauConfirmationComponent,
    InscriptionComponent,
    ConnexionComponent,
    MesReservationsComponent,
    MesReservationsListeComponent,
    MesReservationsItemComponent,
  ],
  exports: [
    NavComponent,
    FooterComponent,
    BureauItemComponent,
    BureauListeComponent
  ],
  imports: [
    CommonModule,
    SiteRoutingModule,
    ReactiveFormsModule
  ]
})
export class SiteModule { }
