import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SiteComponent} from "./site.component";
import {BureauIndexComponent} from "./bureaux/bureau-index/bureau-index.component";
import {BureauDetailsComponent} from "./bureaux/bureau-details/bureau-details.component";
import {
  ReservationBureauCreerComponent
} from "./reservations-bureaux/reservation-bureau-creer/reservation-bureau-creer.component";
import {
  ReservationBureauConfirmationComponent
} from "./reservations-bureaux/reservation-bureau-confirmation/reservation-bureau-confirmation.component";
import {InscriptionComponent} from "./utilisateurs/inscription/inscription.component";
import {ConnexionComponent} from "./utilisateurs/connexion/connexion.component";
import {MesReservationsComponent} from "./mes-reservations/mes-reservations.component";
import {AuthGuard} from "../core/guards/auth.guard";
import {AuthGuardUtilisateur} from "../core/guards/auth-utilisateur.guard";
import {
  MesReservationsListeComponent
} from "./mes-reservations/mes-reservations-liste/mes-reservations-liste.component";

const routes: Routes = [
  {
    path: '', component: SiteComponent, children: [
      {
        path: 'inscription', component: InscriptionComponent
      },
      {
        path: 'connexion', component: ConnexionComponent
      },
      {
        path: '', component: BureauIndexComponent,
      },
      {
        path: 'bureaux/:id', component: BureauDetailsComponent
      },
      {
        path: 'bureaux/:id/reserver', component: ReservationBureauCreerComponent
      },
      {
        path: 'reservations/:id/confirmation', component: ReservationBureauConfirmationComponent
      },
      {
        path: 'mes-reservations', component: MesReservationsListeComponent, canActivate: [AuthGuardUtilisateur]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SiteRoutingModule { }
