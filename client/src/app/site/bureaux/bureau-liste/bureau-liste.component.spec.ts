import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BureauListeComponent } from './bureau-liste.component';

describe('BureauListeComponent', () => {
  let component: BureauListeComponent;
  let fixture: ComponentFixture<BureauListeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BureauListeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BureauListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
