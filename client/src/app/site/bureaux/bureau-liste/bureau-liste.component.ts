import { Component, OnInit } from '@angular/core';
import {BureauxService} from "../../../core/services/bureaux.service";
import {Bureau} from "../../../core/models/bureau.model";
import {FormBuilder} from "@angular/forms";
import {Etage} from "../../../core/models/etage.model";
import {EtagesService} from "../../../core/services/etages.service";
import {FiltrerBureauxDto} from "../../../admin/bureaux/dto/filtrer-bureaux-dto";

@Component({
  selector: 'app-bureau-liste',
  templateUrl: './bureau-liste.component.html',
  styleUrls: ['./bureau-liste.component.scss']
})
export class BureauListeComponent implements OnInit {
  bureaux!: Bureau[];
  etages: Etage[] = [];
  formulaire = this.fb.group({
    etage: [''],
  }, {
    updateOn: 'change'
  });

  get etage() {
    return this.formulaire.get('etage');
  }

  constructor(private bureauxService: BureauxService,
              private etagesService: EtagesService,
              private fb: FormBuilder) {}

  ngOnInit(): void {
    this.bureauxService.obtenirBureaux().subscribe((bureaux) => {
      this.bureaux = bureaux;
    });

    this.etagesService.obtenirEtages().subscribe((etages) => {
      this.etages = etages;
    })

    this.filtrer();
  }

  filtrer() {
    this.formulaire.valueChanges.subscribe((val) => {
      this.bureauxService.filtrerBureaux(this.formulaire.value as FiltrerBureauxDto).subscribe((bureaux) => {
        this.bureaux = bureaux;
      });
    })
  }
}
