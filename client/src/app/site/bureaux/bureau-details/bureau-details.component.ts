import { Component, OnInit } from '@angular/core';
import {Bureau} from "../../../core/models/bureau.model";
import {BureauxService} from "../../../core/services/bureaux.service";
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder} from "@angular/forms";
import {ReservationsService} from "../../../core/services/reservations.service";
import {Reservation} from "../../../core/models/reservation.model";
import {formatDate} from "@angular/common";
import {ReservationsBureauxService} from "../../../core/services/reservations-bureaux.service";

@Component({
  selector: 'app-bureau-details',
  templateUrl: './bureau-details.component.html',
  styleUrls: ['./bureau-details.component.scss']
})
export class BureauDetailsComponent implements OnInit {
  bureau!: Bureau;
  reservations!: Reservation[];
  formulaire = this.fb.group({
    date: [''],
  },
  {
    updateOn: "submit"
  });

  constructor(private bureauService: BureauxService,
              private reservationsService: ReservationsBureauxService,
              private route: ActivatedRoute,
              private fb: FormBuilder,
              private router: Router) { }

  ngOnInit(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.bureauService.obtenirBureau(id).subscribe((bureau) => {
      this.bureau = bureau;
    });

    this.reservationsService.obtenirReservationsPourBureau(id).subscribe((reservations) => {
      this.reservations = reservations;
    })
  }

  reservationExistePourDate(date: string): boolean {
    const format = 'yyyy-MM-dd';
    const locale = 'en-CA';
    date = formatDate(date, format, locale);

    return this.reservations.filter(r => formatDate(r.date, format, locale) === date).length > 0;
  }

  septProchainsJours(): string[] {
    let jours: string[] = [];
    const aujourdhui = new Date();
    let date = new Date(aujourdhui)

    for(let i=0; i<7; i++) {
      date.setDate(date.getDate() + 1)
      jours.push(formatDate(date, 'yyyy-MM-dd', 'en-CA'));
    }

    return jours;
  }

  reserver(date: string) {
    if(date && !this.reservationExistePourDate(date)) {
      this.router.navigate(['/bureaux', this.bureau.id, 'reserver'], {
        queryParams: {
          date: date
        }
      })
    }
  }
}
